from flask import Flask, jsonify, request
import requests
import serial
import serial.tools.list_ports as list_ports
from threading import Thread


PID_MICROBIT = 516
VID_MICROBIT = 3368
TIMEOUT = 10
PORT_RESIDENCIA = 5000
PORT = 7000


def find_comport(pid, vid, baud):
    ser_port = serial.Serial(timeout=TIMEOUT)
    ser_port.baudrate = baud
    ports = list(list_ports.comports())
    print('scanning ports')
    for p in ports:
        print('port: {}'.format(p))
        try:
            print('pid: {} vid: {}'.format(p.pid, p.vid))
        except AttributeError:
            continue
        if (p.pid == pid) and (p.vid == vid):
            print('found target device pid: {} vid: {} port: {}'.format(
                p.pid, p.vid, p.device))
            ser_port.port = str(p.device)
            return ser_port
    return None


app = Flask(__name__)


@app.route("/peticion", methods=["POST"])
def insert_peticion():
    mensaje_details = request.get_json()
    contenido_peticion = mensaje_details["name"]
    ser_micro.write(contenido_peticion.encode('utf-8'))
    print("PETICION: " + contenido_peticion + " Enviada correctamente")
    return "Peticion enviada"


def receive(period, mensaje):
    ser_micro = mensaje
    while 1:
        line = ser_micro.readline().decode('utf-8')
        if line:
            mensaje_microbit = '{"name": "' + line[:-2] + '"}'
            URL = "http://localhost:" + str(PORT_RESIDENCIA) + "/peticion"
            headers = {
                "Content-Type": "application/json"
            }
            print("Mensaje: " + mensaje_microbit)
            peti = requests.post(URL, headers=headers, data=mensaje_microbit)
            print(peti.content)


if __name__ == "__main__":
    global ser_micro
    print('looking for microbit')
    ser_micro = find_comport(PID_MICROBIT, VID_MICROBIT, 9600)
    ser_micro.open()

    t = Thread(target=receive, args=[1, ser_micro])
    t.start()
    app.run(host='0.0.0.0', port=PORT, debug=False)
    ser_micro.close()
